from tempfile import NamedTemporaryFile
import os.path
from suds.client import Client

__author__ = 'arseniy.fomchenko'


def list_soap_methods(url, **kwargs):
    client = Client(url, **kwargs)
    return [_ for _ in client.wsdl.services[0].ports[0].methods]


def list_wsdl_methods_from_file(path, **kwargs):
    assert os.path.isfile(path), '%s is not a file' % path
    return list_soap_methods('file://'+os.path.abspath(path), **kwargs)


def list_wsd_methods_from_string(wsdl_s, **kwargs):
    f = NamedTemporaryFile(delete=True)
    f.write(wsdl_s)
    f.seek(0)
    l = list_wsdl_methods_from_file(f.name, **kwargs)
    f.close()
    return l
