from flask.ext.wtf.file import FileRequired, FileAllowed, FileField
from models import Mock
import wtforms

__author__ = 'arseniy.fomchenko'
from flask_wtf import Form


class Unique(object):
    """ validator that checks field uniqueness """
    def __init__(self, model, columns, message=None):
        self.model = model
        self.columns = columns
        if not message:
            message = u'this element already exists'
        self.message = message

    def __call__(self, form, field):
        check = self.model.query.filter(self.field == field.data).first()
        if check:
            raise wtforms.ValidationError(self.message)


class LoginForm(Form):
    username = wtforms.StringField('username', validators=[wtforms.validators.Regexp('^[\w\.]+$')])
    password = wtforms.PasswordField('password', validators=[wtforms.validators.DataRequired()])
    remember = wtforms.BooleanField('remember me', default=False)


class AddMockForm(Form):
    path = wtforms.StringField('name', validators=[wtforms.validators.DataRequired(),
                                           wtforms.validators.length(max=20),
                                           wtforms.validators.Regexp('^\w+$'),
                                           ])

    wsdl = FileField('wsdl (xml file)', validators=[FileRequired(), FileAllowed(('xml', 'wsdl'), 'only WSDL/XML allowed')])
    patch_locations = wtforms.BooleanField('replace locations in wsdl with its uri', default=True)
    user_id = wtforms.HiddenField('user_id')

    def validate_path(form, path):
        users_mocks = Mock.query.filter_by(user_id=form.user_id.data).all()
        if not reduce(lambda res, _next: res and _next.name != path.data, users_mocks, True):
            raise wtforms.ValidationError('this element already exists')


class AddResponseForm(Form):
    method = wtforms.SelectField('method', coerce=int)
    name = wtforms.StringField('name', validators=[wtforms.validators.Regexp('^\w+$', message='Only alphanumeric & underscore characters allowed')])
    response = wtforms.TextAreaField(u'response')
    user_id = wtforms.HiddenField('user_id')