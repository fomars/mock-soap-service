from flask import Flask
from flask.ext.simpleldap import LDAP
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

ldap = LDAP(app)

import views

if not app.debug:
    import logging
    #mail logging
    from logging.handlers import SMTPHandler
    mail_handler = SMTPHandler(('127.0.0.1', '25'),
                               'error@soapmock.lamoda.ru',
                               app.config['ADMINS'],
                               'Application failed')
    mail_handler.setLevel(logging.ERROR)
    app.logger.addHandler(mail_handler)
    #file logging
    from logging.handlers import RotatingFileHandler
    file_handler = RotatingFileHandler('application.log', 'a', 1 * 1024 * 1024, 10)
    file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'))
    app.logger.setLevel(logging.INFO)
    file_handler.setLevel(logging.INFO)
    app.logger.addHandler(file_handler)
    app.logger.info('Application startup')