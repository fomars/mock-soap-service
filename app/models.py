import datetime
from app import db
import re
from flask.ext.login import UserMixin
from sqlalchemy import func
from sqlalchemy.dialects.mysql import LONGTEXT

__author__ = 'arseniy.fomchenko'



ROLE_USER = 0
ROLE_ADMIN = 1


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True, nullable=False)
    email = db.Column(db.String(120), index=True, unique=True)
    role = db.Column(db.SmallInteger, default=ROLE_USER, nullable=False)

    def __init__(self, username, role=ROLE_USER):
        self.username = username
        self.email = '{0:s}@lamoda.ru'.format(username)
        self.role = role

    def __repr__(self):
        return '<User %r>' % self.username

    @classmethod
    def get(cls, user_id):
        return cls.query.get(user_id)


class Mock(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), nullable=False)
    origin_url = db.Column(db.String(120))
    wsdl = db.Column(LONGTEXT)
    created = db.Column(db.DateTime, default=func.now())
    updated = db.Column(db.DateTime, onupdate=func.now())
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('mocks', lazy='dynamic'))

    __table_args__ = (db.UniqueConstraint('name', 'user_id', name='name_constraint'),)

    def __init__(self, name, wsdl, user, patch):
        if patch:
            self.prepare_wsdl(wsdl)
        else:
            self.wsdl = wsdl
        self.name = name
        self.user = user

    def prepare_wsdl(self, wsdl):
        self.wsdl = re.sub('(<[\S]+ location=")(.+)("/>)', r'\1{{ location }}\3', wsdl)

    def __repr__(self):
        return '<Name %r>' % self.name


class SoapMethod(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    service = db.relationship('Mock', backref=db.backref('methods', lazy='dynamic'))
    service_id = db.Column(db.Integer, db.ForeignKey('mock.id', ondelete='CASCADE'), nullable=False)

    def __init__(self, name, service_id):
        self.name = name
        self.service_id = service_id


class SoapResponse(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    service = db.relationship('Mock', backref=db.backref('responses', lazy='dynamic'))
    service_id = db.Column(db.Integer, db.ForeignKey('mock.id', ondelete='CASCADE'))
    method = db.relationship('SoapMethod', backref=db.backref('responses', lazy='dynamic'))
    method_id = db.Column(db.Integer, db.ForeignKey('soap_method.id', ondelete='CASCADE'))
    content = db.Column(LONGTEXT)
    is_active = db.Column(db.Boolean)
    name = db.Column(db.String(50), default='Untitled')

    def __init__(self, service_id, method_id, name, content):
        self.content = content
        self.service_id = service_id
        self.method_id = method_id
        self.name = name


class LogMsg(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(LONGTEXT)
    service = db.relationship('Mock', backref=db.backref('log_msgs', lazy='dynamic'))
    service_id = db.Column(db.Integer, db.ForeignKey('mock.id', ondelete='CASCADE'))
    added = db.Column(db.DateTime, default=func.now())
    method = db.Column(db.String(60))

    def __init__(self, content, service, method):
        self.content = content
        self.service = service
        self.added = datetime.datetime.now()
        self.method = method.strip('"\'')


