import cgi
import urllib
from urllib2 import URLError
from app.utils import list_wsd_methods_from_string
from flask import url_for, render_template, flash, request, session, render_template_string, Response, jsonify, g
from flask.ext.login import current_user, login_required, login_user, logout_user
from forms import AddMockForm, AddResponseForm, LoginForm
from models import *
from oauth2client import client
from app import app, login_manager, ldap
from werkzeug.utils import redirect

__author__ = 'arseniy.fomchenko'


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)


@app.route('/login', methods=['POST', 'GET'])
# @ldap.basic_auth_required
def login():
    if current_user.is_authenticated:
        flash('You are already logged in')
        return redirect(url_for('index'))

    form = LoginForm()
    if request.method == 'POST' and form.validate():
        username = form.username.data
        pwd = form.password.data
        check = ldap.bind_user(username, pwd)
        if check is None:
            flash('Invalid credentials')
            return redirect(url_for('login'))
        else:
            session['username'] = username
            finish_login(username)
            return redirect(url_for('index'))

    return render_template('login.html',
                           form=form)


@app.route('/foo')
@login_required
def foo():
    username = g.ldap_username
    return 'Welcome, {0}'.format(username)


@app.route('/', methods=["POST", "GET"])
@app.route('/index', methods=["POST", "GET"])
@login_required
def index():
    user = current_user
    form = AddMockForm(user_id=user.id)
    if request.method == 'POST' and form.validate():
        service_name = form.path.data
        wsdl_s = form.wsdl.data.read()
        patch = form.patch_locations.data

        try:
            list_of_mehods = list_wsd_methods_from_string(wsdl_s, timeout=5)
        except URLError:
            flash('Invalid wsdl: some url seem unavailable', category='error')
            return redirect(url_for('index'))

        new_service = Mock(name=service_name,
                           wsdl=wsdl_s,
                           user=current_user,
                           patch=patch)
        db.session.add(new_service)
        db.session.commit()

        for method in list_of_mehods:
            new_method = SoapMethod(name=method, service_id=new_service.id)
            db.session.add(new_method)
        db.session.commit()

        flash('New mock service added!')
        return redirect(url_for('index'))

    services = Mock.query.filter_by(user_id=user.id)

    return render_template("index.html",
                           title='Home',
                           user=user,
                           services=services,
                           form=form)


@app.route('/<username>/<servicename>', methods=['POST', 'GET'])
def service(username, servicename):
    _service = Mock.query.filter_by(user_id=User.query.filter_by(username=username).first().id,
                                    name=servicename).first()

    if request.method == 'POST':
        soap_method = re.split(':|/|#', request.headers.environ.get('HTTP_SOAPACTION', cgi.parse_header(
            request.headers.environ.get('CONTENT_TYPE'))[1].get('action')))[-1].strip('"\'')
        if soap_method:
            soap_method = urllib.quote(soap_method, safe='')
            new_log_msg = LogMsg(content=unicode(request.data, 'utf_8'),
                                 service=_service,
                                 method=soap_method)
            db.session.add(new_log_msg)
            db.session.commit()

            method = SoapMethod.query.filter_by(service=_service, name=soap_method).first()
            resp = SoapResponse.query.filter_by(service=_service, method=method, is_active=True).first()
            resp = resp.content if resp else ''
            return Response(response=resp, status=200, mimetype='text/xml', content_type='text/xml')

    return Response(render_template_string(_service.wsdl,
                                           location=url_for('service',
                                                            username=username,
                                                            servicename=servicename,
                                                            _external=True)),
                    mimetype='text/xml', content_type='text/xml')


@login_required
@app.route('/description/<service_id>')
def description(service_id):
    service = Mock.query.get(service_id)
    if service.user != current_user:
        flash('You are trying to get access to some other user\'s entry')
        return redirect('/index')
    else:
        return render_template('description.html',
                               service=service,
                               methods=service.methods)


@app.route('/delete_service/<_id>')
def delete_sevice(_id):
    Mock.query.filter_by(id=_id).delete()
    LogMsg.query.filter_by(service_id=_id).delete()
    db.session.commit()
    return redirect('/index')


@app.route('/<username>/<servicename>/logs', methods=['DELETE', 'GET'])
def logs(username, servicename):
    _service = Mock.query.filter_by(user_id=User.query.filter_by(username=username).first().id,
                                    name=servicename).first()
    _logs = LogMsg.query.filter_by(service_id=_service.id).all()
    methods = [method.name for method in SoapMethod.query.filter_by(service=_service).all()]
    return render_template('logs.html',
                           username=username,
                           servicename=servicename,
                           logs=_logs,
                           methods=methods)


@login_required
@app.route('/edit/<service_id>', methods=['GET', 'POST'])
def responses(service_id):
    form = AddResponseForm(user_id=current_user.id)
    methods = SoapMethod.query.filter_by(service_id=service_id).all()
    form.method.choices = [(m.id, m.name) for m in methods]
    responses = SoapResponse.query.filter_by(service_id=service_id).all()
    by_methods = {}
    for response in responses:
        try:
            by_methods[response.method.name].append(response)
        except KeyError:
            by_methods[response.method.name] = [response]
    groupped_responses = [{'method_name': key,
                           'responses': value} for key, value in by_methods.iteritems()]

    if request.method == 'POST' and form.validate():
        new_response = SoapResponse(service_id, form.method.data, form.name.data, form.response.data)
        db.session.add(new_response)
        db.session.commit()
        return redirect(url_for('activate_response', _id=new_response.id))
    return render_template('edit.html',
                           service_id=service_id,
                           grouped_responses=groupped_responses,
                           form=form)


@app.route('/response/<_id>/activate')
def activate_response(_id):
    resp = SoapResponse.query.get(_id)
    for response in SoapResponse.query.filter_by(service_id=resp.service.id, method=resp.method).all():
        response.is_active = False
    resp.is_active = True
    db.session.commit()
    return redirect(url_for('responses', service_id=resp.service_id))


@app.route('/response/<_id>')
def response(_id):
    resp = SoapResponse.query.get(_id)
    return resp.content


@app.route('/response/<_id>/delete')
def delete_response(_id):
    resp = SoapResponse.query.get(_id)
    service_id = resp.service_id
    db.session.delete(resp)
    db.session.commit()
    return redirect(url_for('responses', service_id=service_id))


@app.route('/<username>/<servicename>/<methodname>/log')
def log(username, servicename, methodname):
    if methodname == 'None':
        methodname = None
    _service = Mock.query.filter_by(user_id=User.query.filter_by(username=username).first().id,
                                    name=servicename).first()
    _logs = LogMsg.query.filter_by(service_id=_service.id, method=methodname)
    return render_template('logs.html',
                           logs=_logs, )


@app.route('/log_api/<service_id>/<methodname>', methods=['GET', 'DELETE'])
def log_api(service_id, methodname):
    _logs = LogMsg.query.filter_by(service_id=int(service_id), method=methodname).all()
    if request.method == 'GET':
        logs = [{'msg': _log.content,
                 'timestamp': _log.added} for _log in _logs]
        logs.sort(key=lambda _: _['timestamp'])
        return jsonify(logs=logs)
    if request.method == 'DELETE':
        result = LogMsg.query.filter_by(service_id=int(service_id), method=methodname).delete()
        db.session.commit()
        return jsonify(result=result)


@app.route('/oauth2callback')
def oauth2callback():
    flow = client.flow_from_clientsecrets(
        app.config['GOOGLE_CLIENT_SECRET'],
        scope=('profile', 'email'),
        redirect_uri=url_for('oauth2callback', _external=True))
    if 'code' not in request.args:
        auth_uri = flow.step1_get_authorize_url()
        return redirect(auth_uri)
    else:
        auth_code = request.args.get('code')
        credentials = flow.step2_exchange(auth_code)
        session['credentials'] = credentials.to_json()
        finish_login(credentials.id_token['email'].split('@')[0])
        return redirect(url_for('index'))


def finish_login(username):
    # if id_token['email'] is None or id_token['email'] == "":
    #     flash('Invalid login. Email not provided. Please try again.')
    #     return redirect(url_for('login'))
    user = User.query.filter_by(username=username).first()
    if user is None:
        user = User(username=username, role=ROLE_USER)
        db.session.add(user)
        db.session.commit()
    remember_me = False
    if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop('remember_me', None)
    login_user(user, remember=remember_me)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500
