Deploying:
--------------------
1. Clone repository
2. Create virtualenv and activate it ('env' is just a name, you can choose any):
    ```
    $ virtualenv env
    $ . env/bin/activate
    ```
3. Install dependencies:
    ```
    (env)$ pip install -r requirements.txt
    ```
4. Configure your database: in `config.py` edit `SQLALCHEMY_DATABASE_URI`, `DB_USERNAME`, `DB_PWD`
5. Apply DB migrations:
    ```
    (env)$ python manage.py db upgrade
    ```
6. Restart uwsgi:
    ```
    $ sudo service uwsgi restart
    ```