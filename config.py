import os

__author__ = 'arseniy.fomchenko'

basedir = os.path.abspath(os.path.dirname(__file__))
DB_USERNAME = 'afomchenko'
DB_PWD = ''
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{user}:{pwd}@localhost:3306/soapmock'.format(user=DB_USERNAME, pwd=DB_PWD)
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'migrations')
SQLALCHEMY_TRACK_MODIFICATIONS = False

WTF_CSRF_ENABLED = True
SECRET_KEY = 'fuck 0ff'

# debug toolbar
DEBUG_TB_INTERCEPT_REDIRECTS = False

# mail server settings
MAIL_SERVER = 'localhost'
MAIL_PORT = 25
MAIL_USERNAME = None
MAIL_PASSWORD = None

# administrator list
ADMINS = ['arseniy.fomchenko@lamoda.ru']

GOOGLE_CLIENT_ID = '479998272856-0n2i307v1vasgagfdgopi860r4c44dru.apps.googleusercontent.com'
GOOGLE_CLIENT_SECRET = os.path.join(basedir, 'app/static/google_client_secret.json')

# LDAP
LDAP_HOST = 'ad-10.office.lamoda.ru'
LDAP_BASE_DN = 'OU=Offices,DC=office,DC=lamoda,DC=ru'
LDAP_USERNAME = 'cn=s-leos-06,ou=service accounts,dc=office,dc=lamoda,dc=ru'
LDAP_PASSWORD = '2yNfmTi9kXvDOIAN'
LDAP_USER_OBJECT_FILTER = '(&(objectClass=user)(sAMAccountName=%s))'

