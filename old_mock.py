from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import datetime
import socket
from sys import argv
from multiprocessing import Queue
from jinja2 import Environment, FileSystemLoader
import os

__author__ = 'arseniy.fomchenko'


def make_http_handler(path_handlers, msg_queue):
    class SoapHandler(BaseHTTPRequestHandler):
        handlers = dict(
            [('/', lambda: (200, [('Content-type', 'text/html')], 'Hello from SOAP mock server'))] + path_handlers)

        def do_GET(self):
            try:
                handler = self.handlers[self.path]
            except KeyError:
                self.send_response(404)
                self.send_header('Content-type', 'text/html')
                self.send_header('Date', datetime.datetime.now().strftime('%a, %d %b %Y %H:%M:%S %z(%Z)'))
                self.end_headers()
                self.wfile.write('404: path not found')
                return

            response, headers, content = handler()
            self.send_response(response)
            for header in headers:
                self.send_header(*header)
            self.send_header('Date', datetime.datetime.now().strftime('%a, %d %b %Y %H:%M:%S %z(%Z)'))
            self.end_headers()
            self.wfile.write(content)

        def do_POST(self):
            if self.headers.getheader('content-length'):
                content_length = int(self.headers.getheader('content-length'))
            else:
                content_length = 5000
            message = self.rfile.read(content_length)
            msg_queue.put((self.path, message), True, 5)
            self.send_response(200)
            self.send_header('Content-type', 'text/xml')
            self.end_headers()

    return SoapHandler


def get_my_ip():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.connect(('google.com', 80))
    IP = s.getsockname()[0]
    s.close()
    return IP


def render_wsdl(wsdl_template, location):
    folder, filename = os.path.split(wsdl_template)
    jinja_env = Environment(loader=FileSystemLoader(folder))
    template = jinja_env.get_template(filename)
    xml = template.render(location=location)
    return xml


def make_mock_server(port, wsdl_map, msg_queue):
    my_ip = get_my_ip()
    url = u'http://%s:%d' % (my_ip, port)
    
    path_handlers = [('/foo', lambda: (200, [('Content-type', 'text/html')], 'foo'))]

    for path, wsdl_template in wsdl_map:
        wsdl = render_wsdl(wsdl_template, location=url+'/'+path.strip('/'))
        new_handler = lambda content=wsdl: (200, [('Content-type', 'text/xml')], content)
        path_handlers.append((path, new_handler))

    my_soap_handler = make_http_handler(path_handlers, msg_queue)

    httpd = HTTPServer(('', port), my_soap_handler)

    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()


if __name__ == '__main__':
    try:
        port = int(argv[1])
        q = Queue()
        make_mock_server(port, [('/apigw', './example.xml'), ('/wmsgm', './WMSGM.xml')], q)
    except IndexError:
        print('usage: %s port' % os.path.basename(argv[0]))
